package org.riveramdz.nr;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Writes to the console periodically (every 10 seconds) a report of the new unique numbers received
 * and the total of duplicates in that time span and the total number of unique numbers received so far.
 */
class StatsPrinter {
    private static final String REPORTING_LINE = "Received %s Unique Numbers, %s Duplicates, Unique Total : %s";
    private int currentDuplicatesCountValue;
    private int newUniqueNumbersCount;
    private int newDuplicatesCount;
    private AtomicInteger newUniqueNumbers = new AtomicInteger();
    private AtomicInteger duplicatesCount = new AtomicInteger();
    private final NumberServer numberServer;
    int newUniqueNumbersValue;
    Logger logger = LoggerFactory.getLogger(StatsPrinter.class);

    StatsPrinter(NumberServer numberServer) {
        this.numberServer = numberServer;
    }

    AtomicInteger getNewUniqueNumbers() {
        return newUniqueNumbers;
    }

    AtomicInteger getDuplicatesCount() {
        return duplicatesCount;
    }

    Runnable getPrinterThread() {
        return () ->
        {
            int oldUniqueNumbersCount = 0;
            int oldDuplicatesCount = 0;
            while (numberServer.getRequestsAreStillAccepted().get()) {
                try {
                    Thread.sleep(10000);
                    newUniqueNumbersValue = newUniqueNumbers.get();
                    currentDuplicatesCountValue = duplicatesCount.get();
                    newUniqueNumbersCount = newUniqueNumbersValue - oldUniqueNumbersCount;
                    newDuplicatesCount = currentDuplicatesCountValue - oldDuplicatesCount;
                    System.out.println(String.format(REPORTING_LINE, newUniqueNumbersCount, newDuplicatesCount, newUniqueNumbers.get()));
                    oldUniqueNumbersCount = newUniqueNumbersValue;
                    oldDuplicatesCount = currentDuplicatesCountValue;
                } catch (InterruptedException e) {
                    logger.debug("Thread couldn't be put to sleep", e);
                }
            }
        };
    }
}