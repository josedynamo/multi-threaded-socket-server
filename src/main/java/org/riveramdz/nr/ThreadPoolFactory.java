package org.riveramdz.nr;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

class ThreadPoolFactory {
    private ThreadPoolFactory() {
    }

    static ExecutorService getThreadPool(int i, String name) {
        return Executors.newFixedThreadPool(i, new ThreadFactory() {
            private final AtomicInteger instanceCount = new AtomicInteger(1);

            @Override
            public Thread newThread(Runnable r) {
                Thread t = Executors.defaultThreadFactory().newThread(r);
                t.setDaemon(true);
                t.setName(name + instanceCount.getAndIncrement());
                return t;
            }
        });
    }
}