package org.riveramdz.nr;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Socket Server that listen to incoming requests on port 4000 and accepts a maximum of 5 concurrent clients at a time.
 */
class SocketServer {
    private static final int PORT = 4000;
    private final NumberServer numberServer;

    private ExecutorService socketServerThreadPool;

    Logger logger = LoggerFactory.getLogger(SocketServer.class);

    SocketServer(NumberServer numberServer) {
        this.numberServer = numberServer;
    }

    Runnable getServerSocketThread() {
        return () ->
        {
            try (ServerSocket server = new ServerSocket()) {
                server.setReceiveBufferSize(32);
                InetSocketAddress inetSocketAddress = new InetSocketAddress(PORT);
                server.bind(inetSocketAddress, 1000000);
                socketServerThreadPool = ThreadPoolFactory.getThreadPool(5, "WORKER_");
                while (numberServer.getRequestsAreStillAccepted().get()) {
                    final Socket client = server.accept();
                    client.setTcpNoDelay(true);
                    socketServerThreadPool.execute(new Worker(numberServer).workerThread(client));
                }
                numberServer.stop();
            } catch (IOException ex) {
                logger.debug("IO error when accepting socket connection.", ex);
            } catch (RejectedExecutionException ex) {
                logger.debug("Thread Pool is not available", ex);
            }
        };
    }

    ExecutorService getSocketServerThreadPool() {
        return socketServerThreadPool;
    }
}