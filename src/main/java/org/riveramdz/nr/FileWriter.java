package org.riveramdz.nr;

import java.io.BufferedWriter;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Writes periodically (every half second) to a file name numbers.log all
 * the new entries received that are not duplicated.
 */
class FileWriter {
    private final NumberServer numberServer;
    private static final String NUMBERS_LOG = "numbers.log";
    private Logger logger = LoggerFactory.getLogger(FileWriter.class);

    FileWriter(NumberServer numberServer) {
        this.numberServer = numberServer;
    }

    Runnable getFileWriterThread() {
        return () -> {
            try (java.io.FileWriter fileWriter = new java.io.FileWriter(NUMBERS_LOG, false)) {
                BufferedWriter writer = new BufferedWriter(fileWriter);
                while (numberServer.getRequestsAreStillAccepted().get()) {
                    Thread.sleep(500);
                    if (numberServer.getOutToFile().length() > 0) {
                        writer.append(numberServer.getOutToFile().toString());
                        writer.flush();
                        numberServer.getOutToFile().setLength(0);
                    }

                }

            } catch (IOException e) {
                logger.debug("Couldn't write to file", e);
            } catch (InterruptedException e) {
                logger.debug("Error when putting thread to sleep");
            }
        };
    }
}