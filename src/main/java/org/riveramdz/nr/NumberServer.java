package org.riveramdz.nr;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Orchestrates the creation and shutdown of thread pools and threads needed to accept
 * incoming TCP requests from a socket and evaluate if their input is a 9 digit number
 * in which case is processed as required. It gracefully finishes execution allowing all pending
 * tasks to be finished.
 */
public class NumberServer {
    private final ExecutorService screenPrinterThreadPool;
    private final ExecutorService fileWriterSocketPool;
    private final FileWriter fileWriter = new FileWriter(this);
    private final StatsPrinter statsPrinter = new StatsPrinter(this);
    private final SocketServer socketServer = new SocketServer(this);
    private Thread serverThread;

    private Map<String, String> data = new ConcurrentHashMap<>();

    private AtomicBoolean requestsAreStillAccepted = new AtomicBoolean(true);
    private StringBuffer outToFile = new StringBuffer();

    public NumberServer() {
        screenPrinterThreadPool = ThreadPoolFactory.getThreadPool(1, "Stats");
        fileWriterSocketPool = ThreadPoolFactory.getThreadPool(1, "FileWriter");
    }

    public static void main(String[] args) {
        NumberServer server = new NumberServer();
        server.initialize();
        System.out.println("Number Server Started...");
    }

    private void initialize() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));

        Thread listThread = new Thread(statsPrinter.getPrinterThread());
        listThread.setName("Stats");
        screenPrinterThreadPool.execute(listThread);

        Thread fileWriterThread = new Thread(this.fileWriter.getFileWriterThread());
        fileWriterThread.setName("FileWriter");
        fileWriterSocketPool.execute(fileWriterThread);

        serverThread = new Thread(socketServer.getServerSocketThread());
        serverThread.setName("SocketServer");
        serverThread.start();
    }

    public void stop() {
        if (serverThread != null) {
            serverThread.interrupt();
            System.out.println("Shutting down SocketServer Pool...");
            shutDownPools(socketServer.getSocketServerThreadPool());
            System.out.println("Shutting down ScreenStats Pool...");
            shutDownPools(screenPrinterThreadPool);
            System.out.println("Shutting down FileWriter Pool...");
            shutDownPools(fileWriterSocketPool);
        }
        serverThread = null;

    }

    private void shutDownPools(ExecutorService pool) {
        try {
            pool.shutdown();
            if (!pool.awaitTermination(10, TimeUnit.SECONDS)) {
                pool.shutdownNow();
            }
        } catch (InterruptedException e) {
            pool.shutdownNow();
        }
    }

    public AtomicInteger getNewUniqueNumbers() {
        return statsPrinter.getNewUniqueNumbers();
    }

    public AtomicInteger getDuplicatesCount() {
        return statsPrinter.getDuplicatesCount();
    }

    public Map<String, String> getData() {
        return data;
    }

    public AtomicBoolean getRequestsAreStillAccepted() {
        return requestsAreStillAccepted;
    }

    public StringBuffer getOutToFile() {
        return outToFile;
    }

}
