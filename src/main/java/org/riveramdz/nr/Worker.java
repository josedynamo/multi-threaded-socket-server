package org.riveramdz.nr;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/***
 *  Evaluates if the input provided by a socket client is a 9 digit number
 *  or terminates the application when a client sends the word "terminate"
 */
class Worker {
    private final NumberServer numberServer;
    private Logger logger = LoggerFactory.getLogger(Worker.class);

    Worker(NumberServer numberServer) {
        this.numberServer = numberServer;
    }

    Runnable workerThread(Socket client) {
        return () -> {
            try (BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()))) {
                String number = in.readLine();
                if (number.equals("terminate") || NumberChecker.getInstance().acceptsNumber(number)) {
                    if (number.equals("terminate")) {
                        numberServer.getRequestsAreStillAccepted().set(false);

                    } else {
                        if (numberServer.getData().containsKey(number)) {
                            numberServer.getDuplicatesCount().incrementAndGet();
                        } else {
                            addNumberToSolution(number);
                        }
                    }
                }
                client.close();
            } catch (IOException e) {
                logger.debug("IO error during socket connection", e);
            }

        };
    }

    private void addNumberToSolution(String number) {
        numberServer.getData().putIfAbsent(number, number);
        numberServer.getOutToFile().append(number + "\n");
        numberServer.getNewUniqueNumbers().incrementAndGet();
    }
}