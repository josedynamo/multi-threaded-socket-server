package org.riveramdz.nr;

/**
 * Utility class that checks that a string provided is a number made up of exactly 9 digits
 */
class NumberChecker {
    private static int numberOfDigits = 9;
    private static NumberChecker instance;

    static NumberChecker getInstance() {
        if (instance == null) {
            instance = new NumberChecker();
        }
        return instance;
    }

    private NumberChecker() {
    }

    boolean acceptsNumber(String number) {
        return checkLenghtIsNineCharacters(number) && (checkItsANumber(number));
    }

    boolean checkLenghtIsNineCharacters(String number) {
        return number.length() == numberOfDigits();
    }

    int numberOfDigits() {
        return numberOfDigits;
    }

    boolean checkItsANumber(String number) {
        try {
            return Character.isDigit(number.charAt(0))
                    && Character.isDigit(number.charAt(1))
                    && Character.isDigit(number.charAt(2))
                    && Character.isDigit(number.charAt(3))
                    && Character.isDigit(number.charAt(4))
                    && Character.isDigit(number.charAt(5))
                    && Character.isDigit(number.charAt(6))
                    && Character.isDigit(number.charAt(7))
                    && Character.isDigit(number.charAt(8));
        } catch (Exception e) {
            return false;
        }
    }
}
