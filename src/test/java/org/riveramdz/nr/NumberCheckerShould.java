package org.riveramdz.nr;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class NumberCheckerShould {

    @Test
    public void accept_000000000() {
        long startingTime = System.currentTimeMillis();
        for (int i = 0; i <= 10_000_000; i++) {
            assertTrue(NumberChecker.getInstance().checkItsANumber("000000000"));
        }
        long finishingTime = System.currentTimeMillis();

        System.out.println(String.format("Total time taken: %s milliseconds", (finishingTime - startingTime)));
    }

    @Test
    public void accept_numbers_of_nine_digits() {
        long startingTime = System.currentTimeMillis();
        for (int j = 0; j <= 10; j++) {
            for (int i = 0; i <= 2_000_000; i++) {
                assertFalse(NumberChecker.getInstance().acceptsNumber("5555555551"));
                assertFalse(NumberChecker.getInstance().acceptsNumber("1"));
                assertFalse(NumberChecker.getInstance().acceptsNumber("134"));
            }
        }
        long finishingTime = System.currentTimeMillis();

        System.out.println(String.format("Total time taken: %s milliseconds", (finishingTime - startingTime)));
    }

    @Test
    public void reject_numbers_different_of_digits() {
        long startingTime = System.currentTimeMillis();
        for (int j = 0; j <= 10; j++) {
            for (int i = 0; i <= 2_000_000; i++) {
                assertFalse(NumberChecker.getInstance().checkItsANumber("5555555A5"));
                assertFalse(NumberChecker.getInstance().checkItsANumber("A55555555"));
            }
        }
        long finishingTime = System.currentTimeMillis();

        System.out.println(String.format("Total time taken: %s milliseconds", (finishingTime - startingTime)));
    }

}