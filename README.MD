# Number Server

#### How to run the application.

1. You need to install Gradle to build the application and run it.
2. Once you have Gradle installed, go to the root folder of the project 
and execute the command: gradle fatjar.

3. This will create an executable jar in the folder build/libs.

4. Navigate to that folder and run the command: java -jar TechnicalChallenge-1.0.jar

5. The Server will start now and accept a maximum of 5 concurrent clients to process their requests.

6. To stop the server you can press ctrl+C. The application will stop receiving 
but will wait until all the pending tasks are finished.




